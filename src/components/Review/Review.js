import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';

function Review(props){
    const { review } = props

    return (
        <div>
            <Box component="h4" ml={0} border={1} borderRadius={16}>
            <Rating name="rating" value={review.rating} precision={0.5} readOnly/>
                <h5> { review.text }</h5>
                <h4> { review.user.name }</h4>
            </Box>
        </div>
    )
}
    

export default Review; 