import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ReviewList from '../ReviewList/ReviewList';

function Business(props){
    const { business } = props
    const { onClickReturn } = props
    const price = business.price


    
    function Price(){
        const StyledRating = withStyles({
            iconFilled: {
              color: '#85bb65',
            },
            iconHover: {
              color: '#85bb65',
            },
          })(Rating);

        if(price){
            return(
            <StyledRating
            name="customized-color"
            defaultValue={price.length}
            getLabelText={(value) => `${value} Heart${value !== 1 ? 's' : ''}`}
            precision={1}
            readOnly
            icon={<AttachMoneyIcon fontSize="inherit" />}
            />)
        }else{
            return(<div></div>)
        }
    }

    function Categories(){

        const useStyles = makeStyles((theme) => ({
            root: {
              display: 'flex',
              flexWrap: 'wrap',
              '& > *': {
                margin: theme.spacing(0.5),
              },
            },
          }));

          const classes = useStyles();

        if(business.categories){
            return (
                <div className={classes.root}>
                    {business.categories.map((item) => (
                        <Chip key={ item.alias } label={item.title} />
                    ))} 
                </div>
            )
        }
    }

    return (
        <div>
            <Box component="fieldset" ml={2} borderColor="transparent"> 
                <h2> { business.name } </h2>
                <Box component="h4" ml={0} borderColor="transparent"> 
                    <Rating name="rating" value={business.rating} precision={0.5} readOnly/>
                    <Box component="span" ml={2}>
                    { business.review_count } reviews 
                    </Box>
                </Box>
                <Box component="h4" ml={0} borderColor="transparent"> 
                    <Price/>
                </Box>
                <Categories/>
                {business.location.display_address.map((item) => (
                <h4> { item } </h4>
                ))} 
                
                <h4> { business.display_phone } </h4>
                <ReviewList alias={ business.alias } />
                <Button 
                variant="contained" 
                onClick={() => onClickReturn()}>
                    Return to Search
                </Button>
            </Box>   
        </div>    
    )
}
    

export default Business; 