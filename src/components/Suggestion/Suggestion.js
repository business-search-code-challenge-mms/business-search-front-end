import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

function Suggestion(props){
    const { business } = props
    const { onClickBusiness } = props

    return (
        <ListItem button onClick={() => onClickBusiness(business) }>
          <ListItemText primary={ business.name } />
        </ListItem>
    )
}
    

export default Suggestion; 