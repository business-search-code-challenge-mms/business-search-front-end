import { useState } from 'react'
import Review from "../Review/Review";
import BusinessSearchSource from '../../api/BusinessSearchSource'
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';


function ReviewList(props){
  const { alias } = props 

  const [state, setState] = useState({result: {reviews: []}})

  let data = []
    if(state.reviews){
        data = state.reviews || []
    }

  const onClickReview = async () => {
    const reviews = await BusinessSearchSource.get("/business/reviews",{
      params: { alias: alias }
    })
    setState(prevState => {
      return { ...prevState, reviews: reviews.data.reviews }
    })
    console.log(reviews.data.reviews)
  }

  
  return (
    <div>
      <Box component="h4" ml={0} borderColor="transparent">
        <Button 
          variant="contained" 
          onClick={() => onClickReview()}>
          See reviews
        </Button>
      </Box>
        
      {data.map((item) => (
              <Review 
              key={ item.id } 
              review={ item }  
              />
            ))} 
    </div>)


  
}
    

export default ReviewList