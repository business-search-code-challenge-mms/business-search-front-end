import TextField from '@material-ui/core/TextField'
import { useState } from "react"

function SearchBar(props){
    const { onSearch } = props
    const [ searchText, setSearchText ] = useState('')

    const handleInput = (e) => {
        const text = e.target.value
        setSearchText(text)
    }

    const handleKeyPress = (e) => {
        if(e.key === 'Enter') {
            onSearch(searchText)
        }
    }

    return (
        <div>
            <TextField 
            id="outlined-basic" 
            onChange={ handleInput } 
            onKeyPress={ handleKeyPress }
            value={ searchText }
            label="Business Search" 
            variant="outlined" 
            />
        </div>
    );
}
    

export default SearchBar; 