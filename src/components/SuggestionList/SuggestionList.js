import Suggestion from "../Suggestion/Suggestion";
import List from '@material-ui/core/List';


function SuggestionList(props){
  const { results } = props 
  const { onClickBusiness } = props
  
  let data = []
    if(results.data){
        data = results.data.businesses || []
    }
        
    console.log(data)

    return (
      <List component="nav" aria-label="secondary mailbox folders">
        {data.map((item) => (
                <Suggestion 
                key={ item.id } 
                business={ item } 
                onClickBusiness={ onClickBusiness } 
                />
            ))} 
      </List>
    )
}
    

export default SuggestionList