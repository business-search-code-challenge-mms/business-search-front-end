import { useState } from 'react'
import Box from '@material-ui/core/Box'
import SearchBar from '../SearchBar/SearchBar'
import SuggestionList from '../SuggestionList/SuggestionList'
import BusinessSearchSource from '../../api/BusinessSearchSource'
import Business from '../Business/Business'

function App() {
  
  const { location } = require('../../config')
  const { nbMaxResults } = require('../../config')

  const [state, setState] = useState({
    results: []
  })

  const [displayBusiness, setDisplayBusiness] = useState({
    result: {}})

  
  const onSearch = async (text) => {
    const results = await BusinessSearchSource.get("/business/search",{
      params: {term: text, location: location, nbResults: nbMaxResults}
    })
    setState(prevState => {
      return { ...prevState, results: results }
    })
  }


  const onClickBusiness = (business) => {
    console.log(business)
    setDisplayBusiness(prevState => {
      return { ...prevState, result: business }
    })
  }

  const onClickReturn = () => {
    setDisplayBusiness(prevState => {
      return { ...prevState, result: {} }
    })
    setState(prevState => {
      return { ...prevState, results: [] }
    })
  }

  if(Object.keys(displayBusiness.result).length !== 0){
    return (
      <div className="App">
        <Box component="fieldset" ml={3} borderColor="transparent"> 
          <Business 
          business={ displayBusiness.result } 
          onClickReturn={ onClickReturn }
          />
        </Box>
      </div>
    )

  }else{
    return (
      <div className="App">
        <Box component="fieldset" ml={3} borderColor="transparent"> 
          <h2>Search for Businesses in { location }</h2>
          <SearchBar onSearch={ onSearch }/>
          <SuggestionList 
          results={ state.results } 
          onClickBusiness={ onClickBusiness }
          />
        </Box> 
      </div>
    )
  }
  
}

export default App;
