import axios from 'axios';

const { businessSearchURL } = require('../config')

export default axios.create({
    baseURL: businessSearchURL
})