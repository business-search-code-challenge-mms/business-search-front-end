# Business Search Back End

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is the Business Search Front End.
The Business Search Front End allows to search for businesses in Naperville, view detailed information about a business, and see reviews.
The Business Search Front End retrieves business data from the Business Search Back End.
	
## Technologies
Project is created with:
* React.js
	
## Setup
To run this project, first install Business Search Back End:
```
git clone https://gitlab.com/business-search-code-challenge-mms/business-search-back-end.git
```
Start Business Search Back End

Install Business Search Front End locally using npm:
```
git clone https://gitlab.com/business-search-code-challenge-mms/business-search-front-end.git
cd ./business-search-front-end
npm install
npm start
```